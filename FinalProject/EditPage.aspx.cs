﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    //The architecture of this page is based off of Christine Bittle's CRUD basic's EditStudent.aspx.cs
    public partial class EditPage : System.Web.UI.Page
    {
        //retrieve our page id for the WHERE portion in our query
        private int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //This is where we get our page info into the form
        protected DataRowView getPageInfo(int id)
        {
            //Our query string
            string query = "SELECT * FROM pages_list where page_id=" + pageid.ToString();
            //Get our data source to run the query
            page_select.SelectCommand = query;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            //If there is no page in the pageview, invalid id is passed
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }

        //When the edit page button is clicked it runs this method
        protected void Edit_Page(object sender, EventArgs e)
        {
            //Declare strings and put our form information into them
            string pageTitle = page_title.Text;
            string pageContent = page_content.Text;
            string pageAuthor = edit_author._selected_author;
      
            //Take form information and put it into the query string to edit the page
            string editQuery = "Update pages_list set page_title='" + pageTitle + "'," +
                "page_content='" + pageContent + "'," + "page_author='" +pageAuthor+ "' where page_id=" + pageid;
            //Output the query so we know if we mess up the string
            debug.InnerHtml = editQuery;
            edit_page.UpdateCommand = editQuery;
            edit_page.Update();
        }

        //Pre-render method from Christine Bittle

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //If there is no id found then it can't get page information
            DataRowView pagerow = getPageInfo(pageid);
            if(pagerow == null)
            {
                page_name.InnerHtml = "No page found";
                return;
            }
            //Sets the respective form element values to the values in the db
            page_title.Text = pagerow["page_title"].ToString();
            page_content.Text = pagerow["page_content"].ToString();


        }
    }
}