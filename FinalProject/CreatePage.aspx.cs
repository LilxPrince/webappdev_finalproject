﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    public partial class CreatePage : System.Web.UI.Page
    {
        //Query we want to run to make a new student
        private string addQuery = "INSERT INTO pages_list" +
            "(page_id, page_title, page_content, creation_date, page_author)" + "VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void AddPage(object sender, EventArgs e)
        {
            //Set our values so we can input them into the query string
            string pageTitle = page_name.Text.ToString();
            string pageContent = page_content.Text.ToString();
            string pageAuthor = pick_author._selected_author;
            
            //Date of page creation
            //https://docs.microsoft.com/en-us/dotnet/api/system.datetime.now?view=netframework-4.7.2
            DateTime createTime = DateTime.Now;
            
            //Put everything into our query
            addQuery += "((select max(page_id) from pages_list)+1,'" +
                pageTitle+"', '"+pageContent+"', '"+createTime+"', '"+pageAuthor+"')";

            //Show query output
            debug.InnerHtml = addQuery;

            //Tell our SqlDataSource to run the command
            insert_page.InsertCommand = addQuery;
            insert_page.Insert();

        }
    }
}