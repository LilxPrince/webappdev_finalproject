﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="FinalProject.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 id="page_heading" runat="server"></h1>
<%-- Data source for the page title --%>
    <asp:SqlDataSource 
        runat="server"
        id="page_name"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>">
    </asp:SqlDataSource>

    <%--Div query output for page title--%>
    <div id="page_name_query" runat="server"></div>

    <%--Output result from DB of page title query--%>
    <asp:DataGrid ID="page_title_output" runat="server" GridLines="None">
    </asp:DataGrid>

    <%--Data source for the page content--%>
    <asp:SqlDataSource
        runat="server"
        ID="page_content"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>">
    </asp:SqlDataSource>

    <%--Div query output for page content--%>
    <div id="page_content_query" runat="server"></div>

    <%--Output result from DB of page content query--%>
    <asp:DataGrid ID="page_content_output" runat="server" GridLines="None">
    </asp:DataGrid>

    <%-- Data source for the delete --%>
    <asp:SqlDataSource
        runat="server"
        ID="delete_page"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>"></asp:SqlDataSource>
    <%--If the delete button is pressed run the DeletePage method and have a confirmation--%>
    <asp:Button runat="server" ID="delete_page_button"
        OnClick="DeletePage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete Page" />

    <%--Div query output for delete--%>
    <div id="delete_page_query" runat="server"></div>
</asp:Content>
