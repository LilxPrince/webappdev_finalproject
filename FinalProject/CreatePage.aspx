﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreatePage.aspx.cs" Inherits="FinalProject.CreatePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--Page Heading--%>
    <div id="createPageHeading"><h1>Create New Page</h1></div>
    <%--SqlDataSource that runs query to the DB--%>
    <asp:SqlDataSource runat="server" ID="insert_page"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>"></asp:SqlDataSource>
    <%--Page Name Start--%>
    <div>
    <label>Page Name:</label>
    </div>
    <asp:TextBox ID="page_name" runat="server"></asp:TextBox>
    <%--Validation--%>
    <asp:RequiredFieldValidator runat="server" ControlToValidate="page_name" ErrorMessage="Enter a page name">
    </asp:RequiredFieldValidator>
    <%--Page Name End--%>
    <%--Author user control--%>
    <ASP:AuthorPick id="pick_author" runat="server"></ASP:AuthorPick>
    <%--Page Content Start--%>
    <div>
    <label>Page Content:</label>
    </div>
    <div>
        <asp:TextBox ID="page_content" runat="server" width="300px" Height ="100px" TextMode="MultiLine"></asp:TextBox>
    </div>
    <%--Page Content End--%>
    <ASP:Button Text="Add Page" runat="server" OnClick="AddPage"/>
    <%--Debugging div to output query, idea taken from Christine Bittle's CRUD Basics, this appears a lot in the project--%>
    <div runat="server" id="debug"></div>
</asp:Content>
