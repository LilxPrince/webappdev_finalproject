﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    //Reference from Christine Bittle's CRUD basics Students.aspx/Students.aspx.cs
    public partial class Pages : System.Web.UI.Page
    {
        //Query we want to run to see all our pages
        private string basequery = "SELECT page_id, page_title, creation_date, page_author" + " FROM pages_list";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Get our SqlDataSource to run query
            pages_select.SelectCommand = basequery;
            //Bind our data 
            pages_list.DataSource = Pages_Link_Bind(pages_select);
            //pages_query.InnerHtml = basequery;

            //This specifies columns and adds our data to it from the DB query
            BoundColumn page_id = new BoundColumn();
            page_id.DataField = "page_id";
            page_id.HeaderText = "Page ID";
            pages_list.Columns.Add(page_id);

            BoundColumn page_title = new BoundColumn();
            page_title.DataField = "page_title";
            page_title.HeaderText = "Page Title";
            pages_list.Columns.Add(page_title);

            BoundColumn creation_date = new BoundColumn();
            creation_date.DataField = "creation_date";
            creation_date.HeaderText = "Created On";
            pages_list.Columns.Add(creation_date);

            BoundColumn page_author = new BoundColumn();
            page_author.DataField = "page_author";
            page_author.HeaderText = "Page Author";
            pages_list.Columns.Add(page_author);

            //Add an edit button
            BoundColumn edit = new BoundColumn();
            edit.DataField = "Edit";
            edit.HeaderText = "Action";
            pages_list.Columns.Add(edit);

            //Bind our data
            pages_list.DataBind();
        }

        //Manual Bind Method (Students.aspx.cs CRUD basics)
        protected DataView Pages_Link_Bind(SqlDataSource src)
        {
            //Return a dataview to this variable
            DataTable myTable;

            //Bind data to this variable
            DataView myView;

            //Make table that pulls from DataSource 
            myTable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            //Edit link
            DataColumn editColumn = new DataColumn();
            editColumn.ColumnName = "Edit";
            myTable.Columns.Add(editColumn);

            //Add a link in the table so we can go to the page by clicking the title
            foreach (DataRow row in myTable.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["page_title"] =
                    "<a href=\"Page.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["page_title"]
                    + "</a>";

                //For each row add edit column
                row[editColumn] = "<a href=\"EditPage.aspx?page_id=" +
                    row["page_id"] + "\">Edit</a>";
            }

            myView = myTable.DefaultView;

            return myView;
        }

        //When we create a new page the page refreshes
        protected void RedirectCreatePage(object sender, EventArgs e)
        {
            Response.Redirect("CreatePage.aspx");
        }

        //Search functionality that takes the key and adds it to the query
        protected void Search_Pages(object sender, EventArgs e)
        {
            string query = basequery + " WHERE (1=1) ";
            string key = page_key.Text;

            if(key != "")
            {
                query +=
                    " AND page_title LIKE '%" + key + "%'";
            }

            pages_select.SelectCommand = query;
            //search_query.InnerHtml = query;
            pages_list.DataSource = Pages_Link_Bind(pages_select);
            pages_list.DataBind();
        }
    }
}