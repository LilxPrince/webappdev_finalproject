﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="FinalProject.index" %>

<asp:Content ID="Content" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" type="text/css" href="style.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div id="body-content-wrapper">
            <h1>HTTP5101 Final Project</h1>
            <div class="headings">
                <p>Minimum viable product</p>
            </div>
            <ul>
                <li>Can see a list of existing pages in the database (Under “Manage Pages” in the wireframe)</li>
                <li>We can can add pages to the database through the web app</li>
                <li>We can remove pages from the database through the web app</li>
                <li>We can edit pages in the database through the web app </li>
                <li class="notDone">We can interact with a menu that is dynamically generated from the pages in the database </li>
                <li>When we click a menu item we can see the read the page content. </li>
                <li>Website is given a consistent template structure with the user of Master Pages and Content Pages.</li>
                <li>Contains basic validation on inputs for creating and editing pages </li>
            </ul>

            <div class="headings">
                <p>Extra Features</p>
            </div>
            <ul>
                <li>Features which shows the date that the page was published</li>
                <li class="notDone">Feature which allows users to "publish" and "unpublish" pages</li>
                <li class="notDone">Feature which chaanges the menu to a different design on mobile view</li>
                <li class="notDone">Feature which allows users to select a page "Look", which changes the CSS on that page</li>
                <li class="notDone">Feature which allows users to specify an author name</li>
                <li>Feature which creates a different CRUD for "Authors", and when creating/editing a page we can specify from a list of authors</li>
                <li class="notDone">Feature which allows users to specify "Main column 1" and "Main column 2" content text, so that page view shows each piece of content on a separate column</li>
                <li>The ability to search for pages based off page title</li>
            </ul>

            <div class="headings">
                <p>Ending Notes</p>
            </div>
            <div>
                <p>This project is heavily structured and based off of Christine Bittle's CRUD basics project (https://bitbucket.org/salamanderburger/crud-basics/overview). It is expected that most code is referenced from her project. This final project website is an application of the concepts from that project to another scenario. To show my understanding, I will add references and comment explanations.</p>
            </div>
        </div>
</asp:Content>
