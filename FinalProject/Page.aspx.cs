﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    //Reference for this is from Student.aspx/Student.aspx.cs from Christine Bittle's CRUD basics
    public partial class Page : System.Web.UI.Page
    {
        //Get our page ID
        private string pageid
        {
            get { return Request.QueryString["page_id"]; }
        }

        //Queries to run to retrieve our information
        private string page_title_basequery =
            "SELECT page_title AS ' '" + " from pages_list";

        private string page_content_basequery =
            "SELECT page_content AS ' '" + " from pages_list";


        protected void Page_Load(object sender, EventArgs e)
        {
            //If no pageID is found then change the title to no page found
            if(pageid == "" || pageid == null)
            {
                page_heading.InnerHtml = "No page found.";
            }
            else
            {
                //Set end of page title query to find page id 
                page_title_basequery += " WHERE page_id = " + pageid;
                //Set SQL data source to do this query
                page_name.SelectCommand = page_title_basequery;
                //Output query to page for debugging purposes
                //page_name_query.InnerHtml = page_title_basequery;

                //Bind our results to the data table for visual output
                page_title_output.DataSource = page_name;
                page_title_output.DataBind();

                //Repeat of above code except for page content
                page_content_basequery += " WHERE page_id = " + pageid;
                page_content.SelectCommand = page_content_basequery;
                //page_content_query.InnerHtml = page_content_basequery;

                page_content_output.DataSource = page_content;
                page_content_output.DataBind();

            }
        }

        //Delete page method that runs simple query
        protected void DeletePage(object sender, EventArgs e)
        {
            string deleteQuery = "DELETE FROM pages_list WHERE page_id=" + pageid;
            delete_page_query.InnerHtml = deleteQuery;
            delete_page.DeleteCommand = deleteQuery;
            delete_page.Delete();

        }
    }
}