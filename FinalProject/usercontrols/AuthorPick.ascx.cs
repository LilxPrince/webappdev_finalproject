﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject.usercontrols
{
    public partial class AuthorPick : System.Web.UI.UserControl
    {
        //Query to get list of authors
        private string query = "SELECT DISTINCT page_id AS id, page_author FROM pages_list";

        //Technically I realized afterwards that properly done, we should have an authors table
        //which would be joined with the pages table so as this is supposed to be getting author id
        //what it's actually getting is page id

        //Getter and Setter to get the Page ID / Author name
        private int selected_id;
        private string selected_author;
        public int _selected_id
        {
            get { return selected_id; }
            set { selected_id = value; }
        }
        public string _selected_author
        {
            get { return selected_author; }
            set { selected_author = author_pick.SelectedItem.Text; }

        }

        //Page load which has our SqlDataSource run the query as well as bind the DDL data to a view
        //A manual bind method 
        protected void Page_Load(object sender, EventArgs e)
        {
            authors_list_pick.SelectCommand = query;

            Manual_Bind_Authors(authors_list_pick, "author_pick");
        }

        //Manual bind method reference from Christine Bittle's CRUD Basics ClassPick.asxc
        void Manual_Bind_Authors(SqlDataSource src, string dropdown_id)
        {
            //Find DDL
            DropDownList authorList = (DropDownList)FindControl(dropdown_id);

            //Declare/instantiate view
            DataView myView = (DataView)src.Select(DataSourceSelectArguments.Empty);

            //For each row in the view add an entry into the DDL
            //Set the entry text to the author name and set the value of it to the page ID
            //Then finally add it to the list
            foreach (DataRowView row in myView)
            {
                ListItem author_item = new ListItem();
                author_item.Text = row["page_author"].ToString();
                author_item.Value = row["id"].ToString();
                authorList.Items.Add(author_item);
            }
        }

        //This method uses the getter/setter to set the ID and author
        protected void Assign_Author(object sender, EventArgs e)
        {
            _selected_id = int.Parse(author_pick.SelectedValue);
            _selected_author = author_pick.SelectedItem.Text;
        }
    }
}