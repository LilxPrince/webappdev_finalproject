﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{
    public partial class PagesList : System.Web.UI.UserControl
    {
        private string query = "SELECT page_id, page_title FROM pages_list";
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_list_data.SelectCommand = query;
            pages_list.DataSource = Manual_Link_Bind(pages_list_data);
            pages_list.DataBind();
        }

        //Code referenced from CRUD basics Student.aspx.cs
        //Idea behind PagesList was to get menu to work but currently does not show anything
        protected DataView Manual_Link_Bind(SqlDataSource src)
        {
            //Return a dataview to this variable
            DataTable myTable;

            //Bind data to this variable
            DataView myView;

            //Make table that pulls from DataSource 
            myTable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach(DataRow row in myTable.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["page_title"] =
                    "<a href=\"Page.aspx?page_id="
                    + row["page_id"]
                    + "\">"
                    + row["page_title"]
                    + "</a>";
            }
            myView = myTable.DefaultView;

            return myView;
        }
    }
}