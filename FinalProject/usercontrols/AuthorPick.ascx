﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthorPick.ascx.cs" Inherits="FinalProject.usercontrols.AuthorPick" %>

<div id="authorHeading"><label>Pick an author:</label></div>
<%--SqlDataSource that communicates to our database--%>
<asp:SqlDataSource 
    runat="server" 
    ID="authors_list_pick"
    ConnectionString="<%$ ConnectionStrings:final_project_webpages %>">
</asp:SqlDataSource>
<%--Dropdown list control--%>
<asp:DropDownList 
    runat="server" 
    ID="author_pick"
    OnTextChanged="Assign_Author">
</asp:DropDownList>