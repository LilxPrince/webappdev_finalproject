﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagesList.ascx.cs" Inherits="FinalProject.PagesList" %>

    <asp:SqlDataSource runat="server" id="pages_list_data"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages%>">
    </asp:SqlDataSource>
    <div id="listContainer">
            <asp:DataGrid ID="pages_list"
                runat="server" AutoGenerateColumns="False">
            </asp:DataGrid>
    </div>
