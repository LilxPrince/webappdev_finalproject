﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="FinalProject.Pages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%--Search Box--%>
    <ASP:TextBox runat="server" ID="page_key"></ASP:TextBox>
    <ASP:Button runat="server" Text="Search" OnClick="Search_Pages" />

    <!-- Div that outputs query for debugging purposes -->
    <div id="search_query" runat="server">
    </div>
    <div id="debug" runat="server">
    </div>
    <%--User control PagesList--%>
    <ASP:PagesList id="list_pages" runat="server" />

    <%--Manage pages start--%>
    <div id="managePageHeading"><h1>Manage Pages</h1></div>
    <asp:SqlDataSource runat="server" ID="pages_select"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>"></asp:SqlDataSource>
    <div id="pages_query" runat="server">

    </div>
    <asp:DataGrid id="pages_list"
        runat="server" AutoGenerateColumns="False">
    </asp:DataGrid>
    <%--Manage pages end--%>

    <asp:Button runat="server" ID="create_page_button"
        OnClick="RedirectCreatePage"
        Text="Create New Page" />

</asp:Content>
