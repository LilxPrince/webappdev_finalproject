﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FinalProject.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Data source that gets the information to allow us to view existing info in the inputs --%>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages%>">
    </asp:SqlDataSource>

    <%-- Data source which will run an update query --%>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:final_project_webpages %>">
    </asp:SqlDataSource>
    <%--Page Heading--%>
    <div id="editPageHeading"><h1 runat="server" id="page_name">Update </h1></div>
    <%--Page Title Start--%>
    <div id="editPageTitle">
        <div>
        <label>Page Title:</label>
        </div>
        <asp:TextBox ID="page_title" runat="server">
        </asp:TextBox>
        <%--Validation--%>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ErrorMessage="Enter a page name"></asp:RequiredFieldValidator>
    </div>
    <%--Page Title End--%>
    <%--Author user control--%>
    <ASP:AuthorPick id="edit_author" runat="server"></ASP:AuthorPick>
    <%--Edit Page Start--%>
    <div id="editPageContent">
        <label>Page Content:</label>
        <div>
        <asp:TextBox ID="page_content" runat="server" width="300px" Height ="100px" TextMode="MultiLine">
        </asp:TextBox>
        </div>
    </div>
    <%--Edit Page End--%>
    <asp:Button Text="Edit Page" id="edit_page_button" OnClick="Edit_Page" runat="server"/>
    <%--Debug div that outputs query--%>
    <div id="debug" runat="server">
    </div>

</asp:Content>
